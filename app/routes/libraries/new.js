import Ember from 'ember';

export default Ember.Route.extend({
  model() {
   return this.store.createRecord('library');
 },

  //define a model, o controller e demais variáveis necessárias ao utilizar um componente junto de um template form
 setupController: function (controller, model) {
    this._super(controller, model);

    controller.set('title', 'Create a new library');
    controller.set('buttonLabel', 'Create');
  },

  // definindo qual o template que deverá ser utilizado
  // com isso não preciso mais utilizar o template new.hbs default - posso excluir esse template
  renderTemplate() {
    this.render('libraries/form');
  },

  actions: {

   saveLibrary(newLibrary) {
     newLibrary.save().then(() => this.transitionTo('libraries'));
   },

   willTransition() {
     // rollbackAttributes() removes the record from the store
     // if the model 'isNew'
     this.controller.get('model').rollbackAttributes();
   }
 }
});
