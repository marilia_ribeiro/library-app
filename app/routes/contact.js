import Ember from 'ember';

export default Ember.Route.extend({
  model() {
   return this.store.createRecord('contact');
 },

 actions: {
   //gera um alerta dizendo que salvou o e-mail
   sendContact(newContact) {
     newContact.save().then(() => this.transitionTo('admin.contacts'));
    //  newContact.save().then(() => this.transitionToRoute('admin.contacts')); //transitionToRoute é para ser adicionado no controlle e o transitionTo na rota
     //  alert('Sending your message in progress... ');
     //  var email = newContact.get('email');
     //  var message = newContact.get('message');
     // var responseMessage = 'To: ' + email + ', Message: ' + message;
     // this.set('responseMessage', responseMessage);
     // this.set('email', '');
     // this.set('message', '');


   },

   willTransition() {
     // rollbackAttributes() removes the record from the store
     // if the model 'isNew'
     this.controller.get('model').rollbackAttributes();
   }
 }
});
