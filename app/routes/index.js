import Ember from 'ember';

export default Ember.Route.extend({
  model(){
    return this.store.createRecord('invitation');
  },
  actions: {
    //gera um alerta dizendo que salvou o e-mail
    saveInvitation(newInvitation) {
      newInvitation.save();
      this.controller.set('responseMessage', `Thank you! We have just saved your email address: ${newInvitation.get('email')}`);
    },
    willTransition() {
      this.controller.get('model').rollbackAttributes();
    }
  }
});
