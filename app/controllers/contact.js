import Ember from 'ember';

export default Ember.Controller.extend({
  isValidEmail: Ember.computed.match('model.email', /^.+@.+\..+$/),
  isMessageEnoughLong: Ember.computed.gte('model.message.length', 5),

  isValid: Ember.computed.and('isValidEmail', 'isMessageEnoughLong'),
  isDisabled: Ember.computed.not('isValid'),
});
